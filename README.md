# perl

High-level, general-purpose, interpreted, dynamic programming languages. https://perl.org

[[_TOC_]]

# Books
* [*Think Perl 6: How to think like a computer scientist*
  ](https://www.worldcat.org/search?q=ti%3AThink+Perl+6)
  2017 Laurent Rosenfeld and Allen B. Downey
  * Chapter 14. Functional Programming in Perl
    * https://www.oreilly.com/library/view/think-perl-6/9781491980545/ch14.html
    * https://greenteapress.com/thinkperl6/html/thinkperl6015.html
* [*Perl 6 deep dive : data manipulation, concurrency, functional programming, and more*
  ](https://www.worldcat.org/search?q=ti%3APerl+6+Deep+Dive)
  2017 Andrew Shitov
* [*Higher-Order Perl*
  ](https://hop.perl.plover.com/book/)
  2005 Mark Jason Dominus

# Unofficial documentation
* [*The Perl Ambassador: Damian Conway*
  ](https://www.perl.com/article/the-perl-ambassador-damian-conway/)
  2020-10 Mohammad S Anwar

# REPL
* [*How can I start an interactive console for Perl?*
  ](https://stackoverflow.com/questions/73667/how-can-i-start-an-interactive-console-for-perl)
  (2020) stackoverflow

## Build-in
* `perl -de1`
* `rlwrap perl -de1` with readline wrapper

## In libraries or applications
### `perlconsole`
### [Devel::REPL](https://metacpan.org/pod/Devel::REPL)
* [p5sagit/Devel-REPL](https://github.com/p5sagit/Devel-REPL) includes documentation
* [libdevel-repl-perl](https://tracker.debian.org/pkg/libdevel-repl-perl) @Debian

# Compilers
* *"Perlito5" Perl to Java compiler and Perl to JavaScript compiler* [fglock/Perlito](https://github.com/fglock/Perlito)

# Libraries
## Task::Kensho
* [Task-Kensho](https://metacpan.org/release/Task-Kensho) CPAN
* [task-kensho](https://packages.debian.org/search?keywords=task-kensho) Debian

## Parser combinators
* [Parser::Combinators](https://metacpan.org/pod/Parser::Combinators)

## Web-sites back-ends
* [*15+ Perl Frameworks You Should Be Using | 2020 Edition*
  ](https://www.rankred.com/perl-frameworks/)
  2020-01 Varun Kumar

### Popularity
![Perl website's back-ends at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libmojolicious-perl%20libcatalyst-perl%20libcgi-application-perl%20libdancer-perl%20libdancer2-perl%20libembperl-perl%20libpoet-perl&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)
![Perl website's back-ends at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libcatalyst-perl%20libcgi-application-perl%20libdancer-perl%20libdancer2-perl%20libembperl-perl%20libpoet-perl&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

### Mojolicious
* SSE
* WebSocket
* Asynchronous
* Maybe an ancestor of Raku Cro
* FastCGI and many others
* From the same author but more recent than Catalyst and heavier than
  the Dancer (2) microframework (from another author).
